package main;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


/**\
 * Główna klasa aplikacji inicjuje animacje
 * 
 * plik: ConsumerProducerAnimation.java
 * data: 29.12.18
 * @author Paweł Parczyk
 * 
 *
 */
public class ConsumerProducerAnimation extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AnimationPanel panel;
	
	LinkedList<Consumer> consumers = new LinkedList<Consumer>();
	LinkedList<Producer> producers = new LinkedList<Producer>();
	Buffer buf;
	
	ConsumerProducerAnimation() {
		super("Producent Konsumert - Paweł Parczyk 241390");
		panel = new AnimationPanel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 500, 400);		
		this.getContentPane().add(panel);
		
		
		buf = new Buffer(250, 200, 80, new Color(0,0,0), new Color(100,100,100), panel);
		panel.addObject(buf);

		createConsumers();
		createProducer(); 
		
		JMenuBar bar = new JMenuBar();
		JMenu extras = new JMenu("Dodatki");
		JMenuItem about = new JMenuItem("O aplikacji");
		
		extras.add(about);
		bar.add(extras);
		this.setJMenuBar(bar);
		
		about.addActionListener((ActionEvent e) -> {
			JOptionPane.showMessageDialog(this,
					"Autor: Paweł Parczyk 241390\n"
					+ "Aplikacja przedstawia problem producenta i konsumeta \n"
					+ "ze współdzielonym magazynem. \n"
					+ "Zielone kolo producent (pelne z towarem)\n"
					+ "Czerwone kolo konsument (pelne z towarem)\n"
					+ "Czarne kolo magazyn\n"
					+ "Konsumenci i producenci zawsze dostarczaja i odbieraja\n"
					+ "tyle towaru ile wynosi ich promień");
		});
		
		this.setVisible(true);
		
	}
	
	void createConsumers() {
		consumers.add(new Consumer(10, 10, 5, new Color(255,0,0), panel));
		consumers.add(new Consumer(10, 52, 6, new Color(255,0,0), panel));
		
		for(int i=0;i<10;++i) {
			consumers.add(new Consumer(48, 10, new Random().nextInt()%10 + 10, new Color(255,0,0), panel));
		}
		
		Iterator<Consumer> it = consumers.iterator();
		
		while(it.hasNext()) {
			Consumer c = it.next();
			c.setBuffer(buf);
			panel.addObject(c);
			new Thread(c).start();
		}
	}
	
	void createProducer() {
		producers.add(new Producer(100, 10, 10, new Color(0,255,0), panel));
		producers.add(new Producer(100, 10, 15, new Color(0,255,0), panel));
		
		for(int i=0;i<10;++i) {
			producers.add(new Producer(100, 10, new Random().nextInt()%10 + 10, new Color(0,255,0), panel));	
		}
		
		
		Iterator<Producer> it = producers.iterator();
		
		while(it.hasNext()) {
			Producer c = it.next();
			c.setBuffer(buf);
			panel.addObject(c);
			new Thread(c).start();
		}
	}
		
	public static void main(String[] args) {
		ConsumerProducerAnimation anim = new ConsumerProducerAnimation();
		
		while(true) {
		
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			anim.panel.repaint();
		}
		
	}

}
