package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;


/**\
 * Klasa bufora przechowujacego jakies dobro inaczej magazyn
 * 
 * plik: Buffer.java
 * data: 29.12.18
 * @author Paweł Parczyk
 * 
 *
 */
public class Buffer extends AnimationObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int capacity=20;
	int maxCapacity;

	public Buffer(int x, int y, int r, Color c, JPanel parent) {
		super(x, y, r, c, parent);
		// TODO Auto-generated constructor stub
	}
	
	public Buffer(int x, int y, int maxCapacity, Color c, Color fill, JPanel parent) {
		super(x,y,maxCapacity, c, parent);
		
		setFillColor(fill);
		this.maxCapacity = maxCapacity;
		capacity = 20;
	}
	
	//-1 means can not add
	public int add(int goods) {
		if(capacity + goods <= maxCapacity) {
			capacity += goods;
			return goods;
		}
		
		return -1;
	}
	
	//-1 means can not get
	public int get(int goods) {
		
		if(capacity - goods >= 0) {
			capacity -= goods;
			return goods;
		}
		
		return -1;
	}
	
	@Override
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(fillColor());
		g2d.fillOval(getPosX()-capacity, getPosY()-capacity, 2*capacity, 2*capacity);
		
		
		g2d.setColor(color());
		g2d.drawOval(getPosX()-maxCapacity, getPosY()-maxCapacity, 2*maxCapacity, 2*maxCapacity);
	}
	
}
