package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;

import javax.swing.JComponent;
import javax.swing.JPanel;



/**\
 * Klasa definuijaca obiekt animowany w postaci kola o zadanym rozmiarze, 
 * wektorze predkosci, colorze krawedzi i tla a takze pojemnosci 
 * 
 * plik: AniamationObject.java
 * data: 29.12.18
 * @author Paweł Parczyk
 * 
 *
 */

public class AnimationObject extends JComponent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final int maxSpeed = 3;
	private int r = 10;
	private int x;
	private int y;
	private int vx = 5;
	private int vy = 10;
	private Color color;
	private Color fillColor;
	private JPanel parent;
	
	private int capacity;
	
	public AnimationObject(int x, int y, int r, Color c, JPanel parent) {
		setPos(x, y);
		setColor(c);
		fillColor = c;
		this.r = r;
		this.parent = parent;
		
		vx = Math.abs(new Random().nextInt()% maxSpeed + 1);
		vy = Math.abs(new Random().nextInt()% maxSpeed + 1);
	}
	
	public void setFillColor(Color c) {
		fillColor = c;
	}
	
	public void move() {
		x+=vx;
		y+=vy;
		
		
		int mirror = 0;
		
		if(x-r < 0) {
			vx = Math.abs(new Random().nextInt()% maxSpeed + 1);
			mirror = 1;
		}
		else if(x+r > parent.getSize().getWidth()){
			vx = -Math.abs(new Random().nextInt()% maxSpeed + 1);
			mirror = 1;
		}
			
		
		if(y-r < 0) {
			vy = Math.abs(new Random().nextInt()% maxSpeed + 1);
			mirror = 1;
			
		} else if(y+r > parent.getSize().getHeight()) {
			vy = -Math.abs(new Random().nextInt()% maxSpeed + 1);
			mirror = 1;
		}
		
		if(mirror == 1 ) {
			onBorder();
		}
	}
	
	
	//to be overwrite
	public void onBorder() {
		
		if(capacity == 0)
			capacity = r;
		else capacity = 0;
	}
	
	public void setPos(int x, int y) {
		this.x=x;
		this.y=y;
	}

	public void setVelocity(int vx, int vy) {
		this.vx=vx;
		this.vy=vy;
	}
	
	public int getVelocityX() {
		return vx;
	}

	public int getVelocityYy() {
		return vx;
	}
	
	public int getPosX() {
		return x;
	}
	public int getPosY() {
		return y;
	}
	
	public int getR() {
		return r;
	}
	
	public void setColor(Color c) {
		color=c;
	}
	
	public Color color() {
		return color;
	}
	public Color fillColor() {
		return fillColor;
	}
	public JPanel parent() {
		return parent;
	}
	
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		
		
		if(capacity > 0) {
			g2d.setColor(fillColor);
			g2d.fillOval(x-r, y-r, 2*r, 2*r);
		}
		
		
		g2d.setColor(color);
		g2d.drawOval(x-r, y-r, 2*r, 2*r);
	}
	
	public void sleep(int millisecs) {
		try {
			Thread.sleep(millisecs);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int collision(AnimationObject obj) {
		float d = (float) Math.sqrt(Math.pow(x-obj.x, 2) + Math.pow(y-obj.y, 2));
		
		if(d < r + obj.r) return 1; //collision
		else if( d == r+ obj.r) return 0; //touch on border
		else return -1;
	}
	
	public int capacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
}
