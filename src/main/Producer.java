package main;

import java.awt.Color;

import javax.swing.JPanel;


/**\
 * Klasa producenta dobr, dodaje do magazynu jesli jest to mozliwe
 * 
 * plik: Producer.java
 * data: 29.12.18
 * @author Paweł Parczyk
 * 
 *
 */
public class Producer extends AnimationObject implements Runnable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Buffer buf;
	Boolean addOrder = false;
	public Producer(int x, int y, int r, Color c, JPanel parent) {
		super(x, y, r, c, parent);
	}

	@Override
	public void onBorder() {
		// TODO Auto-generated method stub
		
		setCapacity(getR());
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub		
		
		while(true) {
			sleep(100);
			
			int position = collision(buf);
			
			try {
				//no collison
				if(position == -1) {
					move();
				}
				else {
					synchronized(buf){
						
						do {

							sleep(100);
							
							if(position == 1 && capacity() > 0) {
								//get goods from buffer
								if(buf.add(capacity()) != -1) {
									setCapacity(0);
								}
								else buf.wait();
							}
							
							move();
							position = collision(buf);
						}while(position != -1);
						
						buf.notifyAll();
					}
				}
			}
			catch(InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void setBuffer(Buffer buf) {
		this.buf = buf;
	}
	
	public void addTo() {
		addOrder = true;
	}
	
}