package main;

import java.awt.Graphics;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JPanel;

/**
 * Klasa panela na ktorym rysowana jest animacja dostarczania i zabierania produktow
 *  
 * plik: AnimationPanel.java
 * data: 29.12.18
 * @author Paweł Parczyk
 * 
 *
 */

public class AnimationPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	LinkedList<AnimationObject> objects = new LinkedList<AnimationObject>();
	
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        Iterator<AnimationObject> it = objects.iterator();
        
        while(it.hasNext()){

            it.next().draw(g);
        }
    }
	
    public void addObject(AnimationObject obj) {
    	objects.add(obj);
    }
	
}
